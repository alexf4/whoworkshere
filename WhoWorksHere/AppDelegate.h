//
//  AppDelegate.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Employee.h"
#import "EmployeesTableView.h"
#import "Parse/Parse.h"
#import "SingletonArray.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    NSMutableArray *employees;
}

@property (strong, nonatomic) UIWindow *window;

@end
