//
//  EmployeeCell.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EmployeeCell.h"
#import "CustomCellBackground.h"

@implementation EmployeeCell

@synthesize jobLabel, nameLabel, DOB, yearsWithCompany, profilePic;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.backgroundView = [[CustomCellBackground alloc] init] ;
        self.selectedBackgroundView = [[CustomCellBackground alloc] init] ;
        
        // At end of function, right before return cell:
        self.textLabel.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
