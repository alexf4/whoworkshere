//
//  CustomCellBackground.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CustomCellBackground : UIView

-(CGRect) rectFor1PxStroke:(CGRect) rect;

@end
