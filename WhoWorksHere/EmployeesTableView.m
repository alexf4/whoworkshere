//
//  EmployeesTableView.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EmployeesTableView.h"

#import "Employee.h"
#import "EmployeeCell.h"


@implementation EmployeesTableView

@synthesize employees;
@synthesize selectedEmployee;
@synthesize selectedEmployeeRow;
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    employees = [NSMutableArray arrayWithCapacity:20];
   
    SingletonArray *sharedData = [SingletonArray sharedInstance];
    [employees addObjectsFromArray:sharedData.employeeData];
    
    //Register this class to the notifcation center
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData) name:@"dataFinishedLoading" object:nil];
    
    
}
//If the data is late coming in, update with new data
-(void) loadData{
     SingletonArray *sharedData = [SingletonArray sharedInstance];
    [employees removeAllObjects];
    
    [employees addObjectsFromArray:sharedData.employeeData];
    
    [self.tableView reloadData];
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.employees count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    EmployeeCell *cell = (EmployeeCell *)[tableView dequeueReusableCellWithIdentifier:@"EmployeeCell"];
	Employee *employee = [self.employees objectAtIndex:indexPath.row];
	cell.nameLabel.text = employee.name;
	cell.jobLabel.text = employee.jobTitle;
	cell.DOB.text = employee.DOB;
    cell.yearsWithCompany.text = employee.numberOfYears;
    cell.profilePic.image = employee.profile;
    
    cell.backgroundView = [[CustomCellBackground alloc] init] ;
    cell.selectedBackgroundView = [[CustomCellBackground alloc] init] ;
    
    // At end of function, right before return cell:
    cell.textLabel.backgroundColor = [UIColor clearColor];
    

    return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete)
	{
		
        PFQuery *query = [PFQuery queryWithClassName:@"Employees"];
        
        Employee *removerEmployee = [[Employee alloc]init];
        
        removerEmployee = [employees objectAtIndex:indexPath.row];
        
        PFObject *foundEmployee = [query getObjectWithId:removerEmployee.ID];
        
        [foundEmployee deleteInBackground];
        [self.employees removeObjectAtIndex:indexPath.row];
        
         [self.tableView reloadData];
	}   
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.selectedEmployee = [self.employees objectAtIndex:indexPath.row];
    
    self.selectedEmployeeRow = indexPath.row;
    
    [self performSegueWithIdentifier:@"EditEmployee" sender:self];
 
    
}

#pragma mark - PlayerDetailsViewControllerDelegate

- (void)employeeDetailViewControllerDidCancel:(EmployeeDetailViewController *)controller
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)employeeDetailViewControllerDidSave:(EmployeeDetailViewController *)controller
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"AddEmployee"])
	{
		UINavigationController *navigationController = segue.destinationViewController;
		EmployeeDetailViewController *employeeDetailViewController = [[navigationController viewControllers] objectAtIndex:0];
		employeeDetailViewController.delegate = self;
	}
    else if ([segue.identifier isEqualToString:@"EditEmployee"])
	{
		UINavigationController *navigationController = segue.destinationViewController;
		EmployeeDetailViewController *employeeDetailViewController = [[navigationController viewControllers] objectAtIndex:0];

        [employeeDetailViewController setIncomingEmployee:self.selectedEmployee];
		employeeDetailViewController.delegate = self;
	}
}

- (void)employeeDetailViewController:(EmployeeDetailViewController *)controller didAddEmployee:(Employee *)employee
{

    
    PFObject *newEmployee = [PFObject objectWithClassName:@"Employees"];
    [newEmployee setObject:employee.name forKey:@"name"];
    [newEmployee setObject:employee.jobTitle forKey:@"jobTitle"];
    [newEmployee setObject:employee.DOB forKey:@"DOB"];
    [newEmployee setObject:employee.numberOfYears forKey:@"numberOfYears"];
    
    NSData *imageData = UIImagePNGRepresentation(employee.profile);
    PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
    
 
    
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [newEmployee setObject:imageFile forKey:@"profile"];
        
        [newEmployee saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
           PFQuery *query = [PFQuery queryWithClassName:@"Employees"]; 

            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                for (PFObject *user in objects) {
                    if ([[user objectForKey:@"name"] isEqualToString:employee.name]) {
                        employee.ID = [user objectId];
                    }
                }
            }];
        }];
    }];
    
    

	[self.employees addObject:employee];
    
    [self.tableView reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)employeeDetailViewController:(EmployeeDetailViewController *)controller didUpdateEmployee:(Employee *)employee
{
 
    //Find the correct employee
    PFQuery *query = [PFQuery queryWithClassName:@"Employees"];
    [query getObjectInBackgroundWithId:employee.ID 
                                 block:^(PFObject *foundEmployee, NSError *error) {
                                     if (!error) {
                                         [foundEmployee setObject:employee.name forKey:@"name"];
                                         [foundEmployee setObject:employee.jobTitle forKey:@"jobTitle"];
                                         [foundEmployee setObject:employee.DOB forKey:@"DOB"];
                                         [foundEmployee setObject:employee.numberOfYears forKey:@"numberOfYears"];
                                         
                                         NSData *imageData = UIImagePNGRepresentation(employee.profile);
                                         PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];
                                         
                                         [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                                             [foundEmployee setObject:imageFile forKey:@"profile"];
                                             
                                             [foundEmployee saveInBackground];
                                         }];
                                        
                                     } else {
                                         // Log details of our failure
                                         NSLog(@"Error: %@ %@", error, [error userInfo]);
                                     }
                                 }];
    
    [self.employees replaceObjectAtIndex:self.selectedEmployeeRow withObject:employee];
    [self.tableView reloadData];
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
