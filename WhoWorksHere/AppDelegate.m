//
//  AppDelegate.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/23/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;


- (void)customizeAppearance
{
// Create resizable images
UIImage *gradientImage44 = [[UIImage imageNamed:@"surf_gradient_textured_44"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
UIImage *gradientImage32 = [[UIImage imageNamed:@"surf_gradient_textured_32"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];

// Set the background image for *all* UINavigationBars
[[UINavigationBar appearance] setBackgroundImage:gradientImage44 forBarMetrics:UIBarMetricsDefault];
[[UINavigationBar appearance] setBackgroundImage:gradientImage32 forBarMetrics:UIBarMetricsLandscapePhone];

// Customize the title text for *all* UINavigationBars
[[UINavigationBar appearance] setTitleTextAttributes:
 [NSDictionary dictionaryWithObjectsAndKeys:
  [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0], 
  UITextAttributeTextColor, 
  [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8], 
  UITextAttributeTextShadowColor, 
  [NSValue valueWithUIOffset:UIOffsetMake(0, -1)], 
  UITextAttributeTextShadowOffset, 
  [UIFont fontWithName:@"Arial-Bold" size:0.0], 
  UITextAttributeFont, 
  nil]];
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //Set up the parse connection
    [Parse setApplicationId:@"o2cPlUKsMoU5KJKcVk0UO22wIS2Cfh6ABwxsa4Xl" clientKey:@"dowCZExLKHUqx4PnsSZ8pPHMA1Ouy57LGdJJaA5R"];

    //Set up the shared data array
    SingletonArray *sharedData  = [SingletonArray sharedInstance];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Employees"];
    
    //Get the data here so we can start loading the data asap
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *incomingEmp in objects) {
                Employee *employee = [[Employee alloc] init];
                employee.name = [incomingEmp objectForKey:@"name"];
                employee.jobTitle = [incomingEmp objectForKey:@"jobTitle"];
                employee.DOB = [incomingEmp objectForKey:@"DOB"];
                employee.numberOfYears =  [incomingEmp objectForKey:@"numberOfYears"];
                PFFile *imagefile = [incomingEmp objectForKey:@"profile"];
                
                //Needed this here because get data was holding the program up.
                [imagefile getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                    NSData *imagedata = data;
                    employee.profile = [UIImage imageWithData:imagedata];
                    employee.ID = incomingEmp.objectId;
                    [sharedData.employeeData addObject:employee];
                    //once all the data is in, tell the table view to update.
                    if (objects.count == [sharedData.employeeData count]) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"dataFinishedLoading" object:nil];
                       
                    }
                }];        
            }
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];

    [self customizeAppearance];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

@end
