//
//  EmployeeCell.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmployeeCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *jobLabel;
@property (nonatomic, strong) IBOutlet UILabel *DOB;
@property (nonatomic, strong) IBOutlet UILabel *yearsWithCompany;
@property (nonatomic, strong) IBOutlet UIImageView *profilePic;

@end
