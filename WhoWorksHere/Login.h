//
//  Login.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"
#import "EmployeesTableView.h"
#import "loginButton.h"
#import <QuartzCore/QuartzCore.h>

@interface Login : UIViewController{
    loginButton *button;
}
@property (strong, nonatomic) IBOutlet UITextField *userId;
@property (strong, nonatomic) IBOutlet UITextField *userPSW;
@property (strong, nonatomic) IBOutlet UISegmentedControl *loginSetup;
@property (strong, nonatomic) IBOutlet UIButton *goButton;
@property (retain) IBOutlet loginButton *button;

-(IBAction) textFieldDoneEditing : (id) sender;
-(IBAction) go:(id)sender;
-(IBAction)bgTouched:(id)sender;
-(IBAction)segmentChanged:(id)sender;
-(void)signup;
-(void) signin;

@end
