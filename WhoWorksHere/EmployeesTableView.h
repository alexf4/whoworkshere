//
//  EmployeesTableView.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmployeeDetailViewController.h"
#import "Employee.h"
#import "Parse/Parse.h"
#import "SingletonArray.h"
#import "CustomCellBackground.h"

@class EmployeeTableViewController;
@protocol EmployeeTableViewControllerDelegate <NSObject>


@end

@interface EmployeesTableView : UITableViewController <EmployeeTableViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *employees;
@property (nonatomic, strong) Employee * selectedEmployee;
@property  int selectedEmployeeRow;
@property (nonatomic, weak) id <EmployeeTableViewControllerDelegate> delegate;

-(void) getData;
-(void) loadData;
@end
