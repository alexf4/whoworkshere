//
//  SingletonArray.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SingletonArray.h"

@implementation SingletonArray
@synthesize  employeeData;

static SingletonArray * sharedInstance = nil;


+ (SingletonArray *)sharedInstance {
    if (sharedInstance == nil) {
        sharedInstance = [[super allocWithZone:NULL] init];
    }
    
    return sharedInstance;
}
// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init
{
    self = [super init];
    
    if (self) {
        // Work your initialising magic here as you normally would
        employeeData = [NSMutableArray arrayWithCapacity:20];
        
    }
    
    return self;
}


// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
    return self;
}


@end
