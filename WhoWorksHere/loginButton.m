//
//  loginButton.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "loginButton.h"


@implementation loginButton


-(id) initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        _hue = 0.6;
        _saturation = 0.5;
        _brightness = 1.0;
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    
    CGFloat locationList[] = {0.0, 0.5, 1.0};
    CGFloat colorList[] = {
        .5, 0.7, 1.0, 1.0, //red, green, blue, alpha 
        .75, 0.4, 1.0, 1.0, 
        0.0, 0.3, 1.0, 1.0
    };
    

    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, colorList, locationList, num_locations);
    
    CGRect currentBounds = self.bounds;
    CGPoint topCenter = CGPointMake(CGRectGetMinX(currentBounds), 0.0f);
    CGPoint bottom = CGPointMake(CGRectGetMaxX(currentBounds), CGRectGetMinY(currentBounds));
    CGContextDrawLinearGradient(currentContext, glossGradient, topCenter, bottom, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace); 
    

    [self.layer setBorderColor:[UIColor blueColor].CGColor];
    [self.layer setBorderWidth:2.0];
    [self.layer setCornerRadius:10.f];
    [self.layer setMasksToBounds:YES];
   
}



@end
