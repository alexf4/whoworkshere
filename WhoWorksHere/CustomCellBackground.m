//
//  CustomCellBackground.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomCellBackground.h"

@implementation CustomCellBackground

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    
    CGFloat locationList[] = {0.0, 0.5, 1.0};
    CGFloat colorList[] = {
        1.0, 1.0, 1.0, 1.0, //red, green, blue, alpha 
        240.0/255.0, 240.0/255.0, 240.0/255.0, 1.0, 
        
    };

    
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, colorList, locationList, num_locations);
    
    CGRect currentBounds = self.bounds;
    CGPoint topCenter = CGPointMake(CGRectGetMinX(currentBounds), 0.0f);

    CGPoint bottom = CGPointMake(CGRectGetMaxX(currentBounds), CGRectGetMinY(currentBounds));
    CGContextDrawLinearGradient(currentContext, glossGradient, topCenter, bottom, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace); 

    CGRect strokeRect = currentBounds;
    strokeRect.size.height -= 1;
    strokeRect = [self rectFor1PxStroke: strokeRect];
    
    self.layer.borderColor = [UIColor grayColor].CGColor;
    self.layer.borderWidth = 1;
    
}

-(CGRect) rectFor1PxStroke:(CGRect) rect {
    return CGRectMake(rect.origin.x + 0.5, rect.origin.y + 0.5, rect.size.width , rect.size.height);
}


@end
