//
//  EmployeeDetailViewController.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EmployeeDetailViewController;
@class Employee;

@protocol EmployeeDetailViewControllerDelegate <NSObject>
- (void)employeeDetailViewControllerDidCancel: (EmployeeDetailViewController *)controller;
- (void)employeeDetailViewController:(EmployeeDetailViewController *)controller didAddEmployee:(Employee *)employee;
- (void)employeeDetailViewController:(EmployeeDetailViewController *)controller didUpdateEmployee:(Employee *)employee;


@end


@interface EmployeeDetailViewController : UITableViewController <UIImagePickerControllerDelegate>
{
    //Employee *incomingEmployee;
    
    UIImagePickerController *picker;
}
@property (nonatomic, weak) id <EmployeeDetailViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *jobTitleField;
@property (strong, nonatomic) IBOutlet UITextField *YearsWithCompanyField;
@property (strong, nonatomic) IBOutlet UITextField *DOBField;
@property (nonatomic, retain) Employee  *incomingEmployee;
@property (strong, nonatomic) IBOutlet UIImageView *profileImgView;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (IBAction) buttonClicked;
- (BOOL) validateForm;
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;



@end
