//
//  SingletonArray.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SingletonArray : NSObject
{
    NSMutableArray * employeeData;
}

@property (retain, strong) NSMutableArray * employeeData;

+(id)  sharedInstance;

@end
