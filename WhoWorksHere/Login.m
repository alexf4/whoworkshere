//
//  Login.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Login.h"

@implementation Login
@synthesize userId;
@synthesize userPSW;
@synthesize loginSetup;
@synthesize goButton;
@synthesize button;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.button = nil;
    [userId becomeFirstResponder];
    
    userPSW.returnKeyType = UIReturnKeyGo;
        
}


- (void)viewDidUnload
{
    [self setUserId:nil];
    [self setUserPSW:nil];
    [self setLoginSetup:nil];
    [self setGoButton:nil];
    [super viewDidUnload];

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//This IBAction will handle the go button clicked action
//Depending on if the seg
-(IBAction)go:(id)sender{
    if ([loginSetup selectedSegmentIndex] == 0) {
        [self signin];
    }
    else
    {
        //register
        [self signup];
    }
}

-(void) signup{
    NSString *account = [[NSString alloc] initWithString:userId.text];
    NSString *pwd = [[NSString alloc] initWithString:userPSW.text];
    
    PFUser *user = [PFUser user];
    user.username = account;
    user.password = pwd;
    
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            // Hooray! Let them use the app now.
            NSLog(@"signup succesfull");
            
            [loginSetup setSelectedSegmentIndex:0];
            
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Signup Complete"
                                                              message:@"Please click Go."
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            
            [message show];
            
            
        } else {
            NSString *errorString = [[error userInfo] objectForKey:@"error"];
            // Show the errorString somewhere and let the user try again.
            NSLog(@"%d", errorString);
        }
    }];    
}

-(void) signin{
    
    NSString *account = [[NSString alloc] initWithString:userId.text];
    NSString *pwd = [[NSString alloc] initWithString:userPSW.text];
    
    [PFUser logInWithUsernameInBackground:account password:pwd
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            [self performSegueWithIdentifier:@"Advance" sender:self];
                                            
                                        } else {
                                            // the username or password is invalid.
                                            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                              message:@"Invalid username/passowrd"
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:@"OK"
                                                                                    otherButtonTitles:nil];
                                            
                                            [message show];
                                        }
                                    }];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"Advance"])
	{
		UINavigationController *navigationController = segue.destinationViewController;
        EmployeesTableView *nextController = [[navigationController viewControllers] objectAtIndex:0];
        nextController.delegate = self;
	}
}  

- (IBAction)bgTouched:(id)sender {
    
    [userId resignFirstResponder];
    [userPSW resignFirstResponder];
}

-(IBAction) textFieldDoneEditing : (id) sender{
	
    [self go:self];
    [sender resignFirstResponder];
    
}
-(IBAction)segmentChanged:(id)sender{
    
    if ([loginSetup selectedSegmentIndex] == 1) {
        userPSW.placeholder = @"Enter your new password";
        userId.placeholder = @"Enter your new username";
    }
    else
    {
        userPSW.placeholder = @"Password";
        userId.placeholder = @"User ID";  
    }
}


@end
