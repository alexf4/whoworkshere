//
//  EmployeeDetailViewController.m
//  WhoWorksHere
//
//  Created by alex kharbush on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EmployeeDetailViewController.h"
#import "Employee.h"

@implementation EmployeeDetailViewController

@synthesize delegate;
@synthesize nameTextField;
@synthesize jobTitleField;
@synthesize YearsWithCompanyField;
@synthesize DOBField;
@synthesize incomingEmployee;
@synthesize profileImgView;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.nameTextField setText: incomingEmployee.name];
    
    [self.jobTitleField setText:incomingEmployee.jobTitle];
    
    [self.DOBField setText:incomingEmployee.DOB];
    
    [self.YearsWithCompanyField setText: incomingEmployee.numberOfYears] ;
    
    [self.profileImgView setImage:incomingEmployee.profile];
}

- (void)viewDidUnload
{
    [self setNameTextField:nil];
    [self setJobTitleField:nil];
    [self setYearsWithCompanyField:nil];
    [self setDOBField:nil];
    [self setProfileImgView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
		[self.nameTextField becomeFirstResponder];
}

- (IBAction)cancel:(id)sender
{
	[self.delegate employeeDetailViewControllerDidCancel:self];
}
- (IBAction)done:(id)sender
{
    
    //Check to see if data is filled in.
    
    if ([self validateForm]) {
        
        Employee *employee = [[Employee alloc] init];
        employee.name = self.nameTextField.text;
        employee.jobTitle = self.jobTitleField.text;
        employee.DOB = self.DOBField.text;
        employee.numberOfYears =  self.YearsWithCompanyField.text;
        employee.profile = self.profileImgView.image;
    
        //If we are updating then call didUpdateEmployee else call didAddEmployee
        if (incomingEmployee == nil) {
            [self.delegate employeeDetailViewController:self didAddEmployee:employee];
        }
        else
        {
            employee.ID = self.incomingEmployee.ID;
            [self.delegate employeeDetailViewController:self didUpdateEmployee:employee];
        }
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Error"
                                                          message:@"Please enter in all of the data."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
    }
}

-(BOOL) validateForm
{
    BOOL retvalue = TRUE;
    
    if ([self.nameTextField.text length] == 0) {
        retvalue = FALSE;
    }
    else if ([self.jobTitleField.text length] == 0)
    {
        retvalue = FALSE;
    }
    else if ([self.DOBField.text length] == 0)
    {
        retvalue = FALSE;
    }
    else if ([self.YearsWithCompanyField.text length] == 0)
    {
        retvalue = FALSE;
    }
    else if (self.profileImgView.image == nil)
    {
        retvalue = FALSE;
    }
    
    
    
    return retvalue;
}


-(IBAction) buttonClicked {
    
    picker = [[UIImagePickerController alloc] init];
	
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    picker.delegate = self;

	[self presentModalViewController:picker animated:YES];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker {
    
    
    [self dismissModalViewControllerAnimated:YES];
    
}
- (void)imagePickerController:(UIImagePickerController *) Picker

didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *selection = [info objectForKey:UIImagePickerControllerOriginalImage];
    CGSize sizer = [selection size];
    
    //have to scale the image down
    profileImgView.image = [self imageWithImage:selection scaledToSize:CGSizeMake(sizer.width / 10, sizer.height/ 10)];
    
    
    [self dismissModalViewControllerAnimated:YES];
    
}


- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();    
    UIGraphicsEndImageContext();
    return newImage;
}

@end
