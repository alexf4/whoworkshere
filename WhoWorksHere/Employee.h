//
//  Employee.h
//  WhoWorksHere
//
//  Created by alex kharbush on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Employee : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *jobTitle;
@property (nonatomic, copy) NSString *DOB;
@property (nonatomic, copy) NSString* numberOfYears;
@property (nonatomic, copy) UIImage *profile;
@property (nonatomic, copy) NSString *ID;

@end
